package css;

import pixelami.css.CSSParserRule;
import massive.munit.Assert;
import pixelami.css.Tokenizer;
import pixelami.css.Parser;

class CSSStyleRuleResolverTest
{

	var resolver:CSSStyleRuleResolver;
	var stylesheets:Array<StylesheetFile>;

	@BeforeClass
	public function beforeClass():Void
	{

		stylesheets = [];
		var css1 = haxe.Resource.getString("css1");
		var t = new Tokenizer(css1);
		var p = new Parser(t.tokens);
		stylesheets.push({stylesheet:p.stylesheet, file:"css1"});

		var css2 = haxe.Resource.getString("css2");
		var t = new Tokenizer(css2);
		var p = new Parser(t.tokens);
		stylesheets.push({stylesheet:p.stylesheet, file:"css2"});

	}

	@Before
	public function setup():Void
	{
		resolver = new CSSStyleRuleResolver(stylesheets);
	}

	@After
	public function tearDown():Void
	{
	}


	@Test
	public function lastCSSFileShouldTakePrecedenceWhenResolvingSelectors():Void
	{

		// we expect that since css2 defines '.app' then it should override any '.app' selector defined in css1
		var s = resolver.getStyleObject(null, "app");
		//trace(s);

		var value:Dynamic = s.getPrefix("foo").get("fill").value;
		Assert.areEqual("0xFF0000", value);

	}

	@Test
	public function lastTypeSelectorDefinitionShouldTakePrecedence():Void
	{
		var s = resolver.getStyleObject("Text", "h1");
		//trace(s);
		var value:Dynamic = s.getPrefix("foo").get("leading").value;
		Assert.areEqual(-6, value);
	}

	@Test
	public function typeSelectorDefinitionShouldBeOverriddenByClassSelector():Void
	{
		// in this case the 'leading' value defined in '.h2' overrides the 'leading' value defined in 'Text'
		var s = resolver.getStyleObject("Text", "h2");
		var value:Dynamic = s.getPrefix("foo").get("leading").value;
		Assert.areEqual(2, value);
	}

	@Test
	public function styleObjectShouldContainPrefixes():Void
	{
		// in this case the 'leading' value defined in '.h2' overrides the 'leading' value defined in 'Text'
		var s = resolver.getStyleObject("Text", "h2");

		var l = 0;
		for (p in s.prefixes)
		{
			l++;
		}

		Assert.areEqual(1, l);

	}
}
