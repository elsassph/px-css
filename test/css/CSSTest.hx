package css;

import css.helper.TestComponent;
import massive.munit.Assert;
import pixelami.css.Tokenizer;
import pixelami.css.Parser;

class CSSTest
{
	@Test
	public function rulesShouldBeAppliedToTestComponent()
	{
		var c = new TestComponent();
		css.CSS.style(c, "test");

		Assert.areEqual(0xFF0000, c.color);
		Assert.areEqual("left", c.orientation);
		Assert.areEqual(true, c.emphasis);
		Assert.areEqual(60, c.width);
	}
}
