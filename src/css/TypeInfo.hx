package css;

import haxe.macro.Type;

class TypeInfo
{
	static var typeFieldCache:Hash<Hash<ClassField>> = new Hash<Hash<ClassField>>();

	public static function getPropertiesMapFor(type:Type):Hash<ClassField>
	{
		return switch(type) {
			case Type.TInst(t, params):
				var cname = getFQCN(t.get());
				if(!typeFieldCache.exists(cname)) typeFieldCache.set(cname, getPropertiesMap(t.get()));
				typeFieldCache.get(cname);

			case Type.TType(t, params):
				getPropertiesMapFor(haxe.macro.Context.follow(t.get().type));

			default:
				new Hash<ClassField>();
		}
	}

	public static function getTypeNameFor(type:Type)
	{
		return switch(type) {
			case Type.TInst(t, params):
				t.get().name;
			case Type.TType(t, params):
				t.get().name;
			default:
				null;
		}
	}

	static function getFQCN(classType:ClassType):String
	{
		return classType.pack.join(".") + classType.name;
	}

	//TODO this might be optimized by also doing a cacheType lookup
	static function getPropertiesMap(classType:ClassType):Hash<ClassField>
	{
		var properties:Hash<ClassField> = new Hash<ClassField>();
		var fields:Array<ClassField> = classType.fields.get();

		var s = classType.superClass;
		while(s != null)
		{
			var _t:ClassType = s.t.get();
			fields = fields.concat(_t.fields.get());
			s = _t.superClass;
		}

		for(f in fields) {
			switch(f.kind) {
				case FieldKind.FVar(read, write):
					// TODO check if property is writable
					properties.set(f.name, f);
				default:
			}
		}
		return properties;
	}
}


