package css;

import pixelami.css.CSSParserToken.Position;

typedef RuleDeclaration = {prefix:String, name:String, value:Dynamic, pos:Position}

class RuleObject
{
	var prefixMap:Hash<Hash<RuleDeclaration>>;

	public var prefixes(get_prefixes, null):Iterator<String>;
	function get_prefixes():Iterator<String>
	{
		return prefixMap.keys();
	}

	public function new()
	{
		prefixMap = new Hash();
	}

	public function add(prefix:String, name:String, value:Dynamic, pos:Position)
	{
		var declarationMap:Hash<RuleDeclaration> = getPrefix(prefix);
		declarationMap.set(name, {prefix:prefix, name:name, value:value, pos:pos});
	}

	public function getPrefix(prefix:String):Hash<RuleDeclaration>
	{
		if(prefixMap.exists(prefix)) return prefixMap.get(prefix);
		var map:Hash<RuleDeclaration> = new Hash();
		prefixMap.set(prefix, map);
		return map;
	}

}

