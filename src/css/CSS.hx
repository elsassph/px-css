package css;

import haxe.macro.Expr;
import haxe.macro.Context;

class CSS
{
	#if macro

    // Dirty hack to dynamically include a class in a macro.
	static function importClass(fqcn:String)
	{
	    //Context.error("hey: "+trace(fcqn), Context.currentPos());
		var uniqueName = "Importer_" + haxe.Md5.encode(fqcn);

		var sb:StringBuf = new StringBuf();
		var nl = "\n";
		sb.add("package;");
		sb.add(nl);
		sb.add("@:macro class Importer__  {");
		sb.add(nl);
		sb.add("public static function build():haxe.macro.Type {");
		sb.add(nl);
		sb.add(fqcn + ";");
		sb.add(nl);
		sb.add('return haxe.macro.Context.getType("String");');
		sb.add(nl);
		sb.add("}");
		sb.add(nl);
		sb.add("}");
		sb.add(nl);
		sb.add("typedef " + uniqueName + " = haxe.macro.MacroType<[Importer__.build()]>;");
		sys.io.File.saveContent(uniqueName + ".hx", sb.toString());
		Context.getType(uniqueName);
		sys.FileSystem.deleteFile(uniqueName + ".hx");
	}


	static var resolver:CSSStyleRuleResolver;

	public static function styleSheet(cssFiles:Array<String>, ?vendor:String = "")
	{
		var stylesheets = [];
		for (i in 0...cssFiles.length)
		{
			stylesheets.push(StyleUtil.loadStyleSheet(cssFiles[i]));
		}

		resolver = new CSSStyleRuleResolver(stylesheets);
	}

	//static var defaultMode:css.mode.Mode = new css.mode.Mode();
	static var prefixModeMap:css.mode.ModeMap = new css.mode.ModeMap();

	public static function setModeForPrefix(mode:String, prefix:String)
	{
		importClass(mode);
		if(prefix == "default") modeClass = mode;
		var t = Type.resolveClass(mode);
		var inst = Type.createInstance(t, []);
		prefixModeMap.set(prefix, inst);
	}

	static var modeClass:String = "css.mode.DefaultMode";

	public static function setMode(value:String)
	{
		modeClass = value;
		importClass(value);
	}

	public static var mode:css.mode.Mode;

	static function getMode():css.mode.Mode
	{
		if (mode == null)
		{
			var t = Type.resolveClass(modeClass);
			if (t == null)
			{
				haxe.macro.Context.error(
					modeClass + " cannot be resolved. You probably need to add a reference somewhere in the app",
					haxe.macro.Context.currentPos()
				);
			}
			mode = Type.createInstance(t, []);
		}
		return mode;
	}

	public static function eval(className:String)
	{
		if (resolver == null) throw "No resolver instance found. Did you specify e.g. '--macro css.CSS.styleSheet(['app.css'])' in your .hxml ?";

		var styleObject:RuleObject = resolver.getStyleObject(null, className);

		Sys.println("Properties for '" + className + "':");

		for (prefix in styleObject.prefixes)
		{
			var o = styleObject.getPrefix(prefix);
			for (k in o.keys())
			{
				var v =  o.get(k);
				var posInfo = " ("+v.pos.filename+":"+v.pos.lineNumber+":"+v.pos.char+")";
				Sys.println("  " + k + " = " +v.value + posInfo);
			}
		}

	}
	#end

	@:macro public static function style(target:Expr, className:String):Expr
	{
		var pos = Context.currentPos();
		var targetId = null;
		switch(target.expr)
		{
			case EConst(c):
				switch(c)
				{
					case CIdent(s): targetId = s;
					default:
				}
			default:
		}


		var mode:css.mode.Mode = getMode();

		if (mode.isInline())
		{
			return mode.processTarget(target, targetId, className);
		}
		else
		{
			if (resolver == null) throw "No resolver instance found. Did you specify e.g. '--macro css.CSS.styleSheet(['app.css'])' in your .hxml ?";

			var block:Array<Expr> = [];
			var type = Context.typeof(target);
			var typeName = TypeInfo.getTypeNameFor(type);
			var propertiesMap = TypeInfo.getPropertiesMapFor(type);
			var styleObject:RuleObject = resolver.getStyleObject(typeName, className);
			for (prefix in styleObject.prefixes)
			{
				var prefixObject = styleObject.getPrefix(prefix);
				for (field in prefixObject.keys())
				{
					if (!prefixModeMap.exists(prefix)) throw "No mode defined for " + prefix;
					var m = prefixModeMap.get(prefix);

					if (!propertiesMap.exists(field))
					{
						Context.warning(typeName + " does not contain a field called '" + field + "', attempting to remap", pos);
						var e = m.mapField(target, targetId, field, prefixObject.get(field));
						if(e == null) continue;
						block.push(e);
					}
					else
					{
						var e = m.processField(target, targetId, propertiesMap.get(field), prefixObject.get(field));
						block.push(e);
					}
				}
			}

			var e = {
				expr : EBlock(block),
				pos: pos
			}
			return e;
		}
	}
}
