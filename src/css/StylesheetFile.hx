package css;
import pixelami.css.CSSParserRule.Stylesheet;
typedef StylesheetFile =
{
	file:String,
	stylesheet:Stylesheet
}
