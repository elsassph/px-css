package css;

class StyleRule
{
	var prefixedDefinitionsMap:Hash<Hash<Dynamic>>;

	public function new()
	{
		prefixedDefinitionsMap = new Hash<Hash<Dynamic>>();
    }

	public function addDefinition(prefix:String, name:String, value:Dynamic)
	{
	    if(!prefixedDefinitionsMap.exists(prefix))
		{
			prefixedDefinitionsMap.set(prefix, new Hash<Dynamic>());
		}
		var definitions = prefixedDefinitionsMap.get(prefix);
		definitions.set(name ,value);
	}

	public function getDefinitionsForPrefix(prefix:String):Hash<Dynamic>
	{
		return prefixedDefinitionsMap.get(prefix);
	}

	public function getPrefixes():Iterator<String>
	{
		return prefixedDefinitionsMap.keys();
	}
}
