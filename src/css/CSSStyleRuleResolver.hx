package css;

import css.RuleObject.RuleDeclaration;
import pixelami.css.CSSParserToken;
import pixelami.css.CSSParserRule;
import pixelami.css.Tokenizer;
import pixelami.css.Parser;

class CSSStyleRuleResolver
{
	static inline var DEFAULT_PREFIX:String = "default";
	var stylesheets:Array<StylesheetFile>;

	public function new(stylesheets:Array<StylesheetFile>)
	{
		this.stylesheets = stylesheets;
		//warn("styles "+stylesheets);
	}

	public function getStyleObject(typeName:String, styleName:String):RuleObject
	{
		// First resolve any rule that might apply to the Type.
		var typeRule = ruleMatch(typeName);
		var style:RuleObject = typeRule == null ? new RuleObject() : typeRule;

		// Now resolve the rules for the selectors.
		var selectors = parseStyleName(styleName);
		//warn("selectors: "+selectors);
		for (selector in selectors)
		{
			var ruleObject:RuleObject = ruleMatch("." + selector);
			if (ruleObject == null)
			{
				warn(selector + " not found");
				continue;
			}

			for (prefix in ruleObject.prefixes)
			{
				var target:Hash<RuleDeclaration> = style.getPrefix(prefix);
				var value:Hash<RuleDeclaration> = ruleObject.getPrefix(prefix);

				for (key in value.keys())
				{
					target.set(key, value.get(key));
				}

			}
		}
		return style;
	}

	function parseStyleName(styleName:String):Array<String>
	{
		return styleName.split(" ");
	}

	function ruleMatch(selector:String):RuleObject
	{
		if (selector == "") return null;
		var i = stylesheets.length;
		while (--i >= 0)
		{
			var stylesheetFile = stylesheets[i];

			for (rule in stylesheetFile.stylesheet.value)
			{
				//trace("selector search: " + selectorValueToString(rule.selector));
				//warn("string: "+rule.selector);

				if (selector == selectorValueToString(rule.selector)) {

					return ruleObject(rule, stylesheetFile.file);
				}
			}
		}

		return null;
	}

	function selectorValueToString(val:Array<pixelami.css.CSSParserToken>):String
	{
		if (val[0].tokenType == TokenType.IDENTIFIER) return val[0].value;
		if (val[0].tokenType == TokenType.DELIMITER && val[0].value == 46) return "." + val[1].value;

		return null;
	}


	function ruleObject(rule:CSSParserRule, file:String):RuleObject
	{
		var ruleObject = new RuleObject();
		for (item in rule.value)
		{
			if(Std.is(item, pixelami.css.Declaration))
			{
				var declaration:Declaration = cast item;
				declaration.pos.filename = file;
				var prefixAndName = splitPrefixAndName(declaration.name);
				var name = parseRuleItemName(prefixAndName.name);
				var token = getFirstNonWhitespaceToken(cast declaration.value);
				if(token == null) throw "Token Expected - " + declaration.value;
				var value = parseTokenValue(token);
				//warn("prefix: "+prefixAndName.prefix);
				if (prefixAndName.prefix == "") ruleObject.add("default", name, value, declaration.pos);//warn("ignoring item with no prefix " + item);
				else ruleObject.add(prefixAndName.prefix, name, value, declaration.pos);
			}
			else
			{
				warn("Currently only supporting CSS Declarations - unsupported:" + Type.getClassName(Type.getClass(item)));
			}
		}
		return ruleObject;
	}

	function hasPrefix(name:String):Bool
	{
		return StringTools.startsWith(name, "-");
	}

	function splitPrefixAndName(name:String):{prefix:String, name:String}
	{
		if (hasPrefix(name))
		{
			var idx = name.indexOf("-", 1);
			var prefix = name.substr(1, idx - 1);
			var n = name.substr(idx + 1);
			return {prefix:prefix, name:n};
		}
		return {prefix:DEFAULT_PREFIX, name:name};
	}

	function parseRuleItemName(name:String):String
	{
		// converts hyphenated names to camelCase
		var p:EReg = ~/-([a-z])/;
		var n = p.customReplace(name, function(e:EReg):String
		{
			return e.matched(1).toUpperCase();
		});

		return n;
	}

	function getFirstNonWhitespaceToken(tokens:Array<pixelami.css.CSSParserToken>):CSSParserToken
	{
		var i = -1;
		while (i++ < tokens.length)
		{
			if (tokens[i].tokenType != TokenType.WHITESPACE)
			{
				return tokens[i];
			}
		}
		return null;
	}

	function parseTokenValue(token:CSSParserToken):Dynamic
	{
		//warn("token: "+token);
		var value:Dynamic = token;

		try
		{
			value = switch(token.tokenType)
			{
				case TokenType.HASH : "0x" + token.value;
				case TokenType.NUMBER : token.value;
				case TokenType.DIMENSION : untyped token.num;
				case TokenType.STRING : "\"" + token.value + "\"";
				case TokenType.IDENTIFIER : token.value;
				default: null;
			}
		}
		catch(e:Dynamic)
		{
		    warn(e);
		}
		return value;
	}

	function warn(message:String)
	{
		//trace(message);
		#if macro haxe.macro.Context.warning(message, haxe.macro.Context.currentPos()); #end
	}
}
