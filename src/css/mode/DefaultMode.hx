package css.mode;
#if macro
import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;

class DefaultMode implements Mode
{
	public function new()
	{
	}

	/*
	@return if true then processTarget is called, if false then processField is called.
	 */
	public function isInline():Bool
	{
		return false;
	}

	public function processTarget(target:Expr, targetId:String, className:String):Null<Expr>
	{
		return null;
		// if we were delegating to an HtmlDom element and we want the browser to process
		// the style class, we could do something like.
		// e.g.
		// Context.parse(targetId + '.setAttribute("class",'+className+')', Context.currentPos());
		//
		// and return isInline = true
	}

	public function processField(target:Expr, targetId:String, field:ClassField, cssDeclaration:Dynamic):Null<Expr>
	{
		if(Std.is(cssDeclaration.value, String) || Std.is(cssDeclaration.value, Float) || Std.is(cssDeclaration.value, Int))
		{
			return haxe.macro.Context.parse(targetId + "." + field.name + ' = '+cssDeclaration.value, haxe.macro.Context.currentPos());
		}
		// if we got here then we need to write some custom logic for processing the cssDeclaration
		Context.warning("Unhandled cssDecalation:" + cssDeclaration, Context.currentPos());
		return null;
	}

	public function mapField(target:Expr, targetId:String, fieldName:String, cssValue:Dynamic):Null<Expr>
	{
		return null;
	}
}
#end