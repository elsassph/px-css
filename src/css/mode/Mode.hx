package css.mode;

import haxe.macro.Type;
import haxe.macro.Expr;

interface Mode
{
	function isInline():Bool;
	function processTarget(target:Expr, targetId:String, className:String):Null<Expr>;
	function processField(target:Expr, targetId:String, field:ClassField, cssDeclaration:Dynamic):Null<Expr>;
	function mapField(target:Expr, targetId:String, fieldName:String, cssDeclaration:Dynamic):Null<Expr>;
}
