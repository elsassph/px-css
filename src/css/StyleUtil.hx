package css;

import pixelami.css.CSSParserRule;
import pixelami.css.Tokenizer;
import pixelami.css.Parser;

class StyleUtil
{
    static var styleCache:Hash<Stylesheet> = new Hash<Stylesheet>();

    public static function loadStyleSheet(url:String):StylesheetFile
    {
        var _style:StylesheetFile = {file:url, stylesheet:null};
        if(styleCache.exists(url)) _style.stylesheet = styleCache.get(url);
        else
        {
            var content = sys.io.File.getContent(url);
            _style.stylesheet = parseCSS(content);
            styleCache.set(url, _style.stylesheet);
        }
        return _style;
    }
    public static function parseCSS(str:String):Stylesheet
    {
        //trace(str);
        var t = new Tokenizer(str);
        var p = new Parser(t.tokens);
		//trace(p.stylesheet);
        return p.stylesheet;
    }
}


